import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";

import AppTemplate from './commons/ui/components/AppTemplate';
import AsyncComponent from './AsyncComponent';
import TABS from './tabs.json';
import "./style/app.scss";

TABS.sort(({ firstOrder }, { nextOrder }) => (firstOrder === nextOrder ? 0 : firstOrder - nextOrder));

const components = TABS.map(({ id, path }) =>
  (() => import(/* webpackChunkName: `chunks` */ `./${path}`)));

const ROUTES = TABS.sort().map((data, index) =>
  (<Route key={index} path={`/${data.id}`} exact={true}
          component={() => <AsyncComponent moduleProvider={components[index]}/>}/>));

render(
  <Router>
    <AppTemplate>
      <Switch>
        {ROUTES}
        <Redirect from="/" to={'/' + TABS[0].id}/>
      </Switch>
    </AppTemplate>
  </Router>,
  document.getElementById("app")
);