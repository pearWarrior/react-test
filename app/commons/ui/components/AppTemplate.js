import React from 'react';
import { Link } from 'react-router-dom';
import TABS from './../../../tabs.json'

TABS.sort(
  ({ order: firstOrder }, { order: nextOrder }) => (firstOrder === nextOrder ? 0 : firstOrder - nextOrder)
);

const LINKS = TABS.map(({ id, title }) => <Link key={id} to={id}>{title}</Link>);

export default ({ children }) => (
  <div className="flex-container">
    <nav>
      {LINKS}
    </nav>
    <div className="container">
      <div className="card">
        {children}
      </div>
    </div>
    <hr/>
  </div>
);